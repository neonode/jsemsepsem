// BOUNDS BASED
// Marker format is LAT, LNG, STATUS

require("./main.scss");

// DEBUG

const DEBUG_MODE_ENABLED = true;

function debug(message) {
  if (DEBUG_MODE_ENABLED) {
    console.log(message);
  }
}

// END DEBUG

var map,
  startMarker,
  infobox,
  clickedMarker,
  autocomplete,
  navigationLink,
  directionsDisplay,
  userPosition,
  markers = [],
  source = document.getElementById("infobox-template"),
  template = source.innerHTML,
  boxText = document.createElement("div"),
  center = {
    lat: 50.085132,
    lng: 14.423484
  },
  image_url = [
    themePath + "/img/icon-empty.png",
    themePath + "/img/icon-half.png",
    themePath + "/img/icon-full.png",
    themePath + "/img/icon-start.png"
  ],
  status_strings = ["Empty", "Running out", "Full"];

boxText.style.cssText = "margin-top: 8px; background: #fff; padding: 0px;";
boxText.innerHTML = template;

// Set all map options
var mapOptions = {
  zoom: 15,
  center: center,
  zoomControl: true,
  mapTypeControl: false,
  scaleControl: false,
  streetViewControl: false,
  rotateControl: false,
  fullscreenControl: false,
  styles: [
    {
      featureType: "poi",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }
  ]
};

// Map initialize function is run in callback
window.initMap = function() {
  // Add infobox script
  var head = document.getElementsByTagName("head")[0];
  var s = document.createElement("script");
  s.type = "text/javascript";
  s.src = themePath + "/js/vendor/infobox.js";
  head.appendChild(s);

  map = new google.maps.Map(document.getElementById("map"), mapOptions);

  var geocoder = new google.maps.Geocoder();

  autocomplete = new google.maps.places.Autocomplete(document.getElementById("autocomplete"), {types: ["geocode"]});

  autocomplete.addListener("place_changed", onPlaceChanged);

  function onPlaceChanged() {
    var place = autocomplete.getPlace();
    if (place.geometry) {
      map.panTo(place.geometry.location);
      map.setZoom(15);
    }
  }

  // Initialize directions API
  var directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer({map: map, suppressMarkers: true});

  // Infobox options
  var ibOptions = {
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-100, 0),
    zIndex: null,
    boxStyle: {
      padding: "0px 0px 0px 0px",
      width: "200px",
      height: "60px"
    },
    closeBoxURL: "",
    infoBoxClearance: new google.maps.Size(1, 1),
    isHidden: false,
    pane: "floatPane",
    enableEventPropagation: false
  };

  // Create marker for starting point in mavigation
  makeStartMarker();
  addYourLocationButton(map);

  // Try HTML5 geolocation and if true - set map center to user position and set start marker position
  getPositionPromise().then(position => {
    userPosition = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    map.setCenter(userPosition);
    setStartMarker(userPosition);
  }).catch(err => {
    console.error(err.message);
  });

  // Add event listener to track when the map is idle
  google.maps.event.addListener(map, "idle", function() {
    // go thru all locations and create markers
    for (var i = 0; i < locations.length; i++) {
      var location = locations[i];
      var myLatLng = new google.maps.LatLng(location[0], location[1]),
        marker = new google.maps.Marker({
          position: myLatLng,
          draggable: false,
          icon: {
            url: checkLocationStatus(location[2], image_url),
            size: new google.maps.Size(34, 52),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 52)
          },
          metadata: {
            id: createUniqueId(location[0], location[1]),
            status: checkLocationStatus(location[2], status_strings),
            address: location[3],
            dirURL: `https://www.google.com/maps/dir/?api=1&travelmode=walking&dir_action=navigate&destination=${location[0]}%2C+${location[1]}`
          },
          optimized: false,
          visible: false
        });

      addInfoBox(marker, geocoder);
      // Keep marker instances in a global array
      checkAndAdd(marker);
    }

    // get map bounds and display markers that are in bounds
    var bounds = map.getBounds();
    showVisibleMarkers(bounds);
  });

  function addInfoBox(marker, geocoder) {
    google.maps.event.addListener(marker, "click", function(marker) {
      clearInfoBoxes();

      clickedMarker = this;
      var streetName;

      function getLocationData(marker, callback) {
        if (clickedMarker.metadata.address === undefined) {
          geocoder.geocode({
            location: marker.latLng
          }, function(results, status) {
            if (status === "OK") {
              if (results[1]) {
                clickedMarker.metadata.address = results[0].address_components[2].short_name;
                callback((streetName = results[0].address_components[2].short_name));
              }
            } else {
              window.alert("Geocoder failed due to: " + status);
            }
          });
        } else {
          doInfoBox((streetName = clickedMarker.metadata.address), (status = clickedMarker.metadata.status));
        }
      }

      ibOptions.content = boxText;

      function doInfoBox() {
        infobox = new InfoBox(ibOptions);
        infobox.open(map, clickedMarker);
        var closeInfobox = infobox.content_.children["0"].children[6];
        closeInfobox.addEventListener("click", function() {
          clearInfoBoxes();
        });
        infobox.content_.childNodes[1].childNodes[1].innerHTML = streetName;
        infobox.content_.childNodes[1].childNodes[3].innerHTML = status;
        map.panTo(infobox.getPosition());

        // Add navigation click
        navigationLink = infobox.content_.children["0"].children[2];

        infobox.addListener("domready", function() {
          navigationLink.addEventListener("click", launchNavigation);
        });

        function launchNavigation(e) {
          navigationLink.removeEventListener("click", launchNavigation);
          e.preventDefault();
          clearInfoBoxes();

          getPositionPromise().then(position => {
            var baseDirectionsUrl = clickedMarker.metadata.dirURL + "&origin=" + position.coords.latitude + "%2C+" + position.coords.longitude;
            openInNewTab(baseDirectionsUrl);
          }).catch(err => {
            console.error(err.message);
          });
        }
      }

      getLocationData(marker, doInfoBox);
    });
  }

  google.maps.event.addListener(map, "click", function(e) {
    clearInfoBoxes();
  });
}

// Clear infoboxes
function clearInfoBoxes() {
  if (infobox) {
    infobox.close();
  }
}

// check if marker is in bounds and show / hide it
function showVisibleMarkers(bounds) {
  for (var i = 0; i < markers.length; i++) {
    var marker = markers[i];
    if (bounds.contains(marker.getPosition()) === true) {
      if (marker.visible === false) {
        marker.setMap(map);
        marker.setVisible(true);
      }
    } else {
      marker.setMap(null);
      marker.setVisible(false);
    }
  }
}

// Check the status of the location and return image url for icon
function checkLocationStatus(status, dataArray) {
  switch (status) {
    case 2:
      return dataArray[1];
      break;
    case 3:
      return dataArray[2];
      break;
    default:
      return dataArray[0];
  }
}

// create unique id by concatinating latitude and longitude
function createUniqueId(lt, ln) {
  return ((lt.toString() + ln.toString()).replace(/\./g, ""));
}

// if marker not in array - push it
function checkAndAdd(marker) {
  var found = markers.some(function(el) {
    return el.metadata.id === marker.metadata.id;
  });
  if (!found) {
    markers.push(marker);
  }
}

// calculate and display directions from a to b
function calculateAndDisplayRoute(directionsService, directionsDisplay, userPosition, marker) {
  directionsService.route({
    origin: userPosition,
    destination: marker,
    avoidHighways: false,
    travelMode: google.maps.TravelMode.WALKING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      setStartMarker(userPosition);
      directionsContainer.classList.add("active");
      distanceContainer.innerHTML = directionsDisplay.directions.routes["0"].legs["0"].distance.text;
      durationContainer.innerHTML = directionsDisplay.directions.routes["0"].legs["0"].duration.text;
    } else {
      window.alert("Directions request failed due to " + status);
    }
  });
  directionsDisplay.setMap(map);
}

function makeStartMarker() {
  startMarker = new google.maps.Marker({map: map, icon: image_url[3], visible: false});
}

function setStartMarker(position) {
  startMarker.setPosition(position);
  startMarker.setVisible(true);
}

function addYourLocationButton(map) {
  var locationImage = document.getElementById("your-location-img");
  var locationButton = document.getElementById("location-button");

  google.maps.event.addListener(map, "dragend", function() {
    locationImage.style.backgroundPosition = "0px 0px";
  });

  locationButton.addEventListener("click", function() {
    var imgX = "0";
    var animationInterval = setInterval(function() {
      if (imgX === "-18") 
        imgX = "0";
      else 
        imgX = "-18";
      locationImage.stlye.backgroundPosition = imgX + "px 0px";
    }, 500);
    map.setCenter(userPosition);
    clearInterval(animationInterval);
    locationImage.style.backgroundPosition = "-144px 0px";
  });
}

var geo_options = {
  maximumAge: 30000,
  timeout: 27000
};

var reject = function(position) {
  console.log("Error occurred. Error code: " + error.code);
  // error.code can be:
  //   0: unknown error
  //   1: permission denied
  //   2: position unavailable (error response from location provider)
  //   3: timed out
};

var getPositionPromise = function(options) {
  return new Promise(function(resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject, geo_options);
  });
};

function openInNewTab(url) {
  var win = window.open(url, "_blank");
  win.focus();
}

// MAIN MENU

var hamburger = document.getElementById("main-menu");
var main_menu = document.getElementById("navigation");
hamburger.addEventListener("click", function() {
  this.classList.toggle("is-active");
  main_menu.classList.toggle("is-open");
  if (main_menu.classList.contains("is-open")) {
    main_menu.classList.add("is-closing");
  }
  var x = this.getAttribute("aria-expanded");
  if (x === "true") {
    x = "false";
  } else {
    x = "true";
  }
  this.setAttribute("aria-expanded", x);
}, false);

// Autocomplete form

var s = document.getElementById("autocomplete"),
  f = document.getElementById("locationField"),
  a = document.getElementById("after");

s.addEventListener("focus", function() {
  if (f.classList.contains("open")) 
    return;
  f.classList.add("in");
  setTimeout(function() {
    f.classList.add("open");
    f.classList.remove("in");
  }, 800);
});

a.addEventListener("click", function(e) {
  e.preventDefault();
  if (!f.classList.contains("open")) 
    return;
  s.value = "";
  f.classList.add("closed");
  f.classList.remove("open");
  setTimeout(function() {
    f.classList.remove("closed");
  }, 800);
});

function recreateNode(el, withChildren) {
  if (withChildren) {
    el.parentNode.replaceChild(el.cloneNode(true), el);
  } else {
    var newEl = el.cloneNode(false);
    while (el.hasChildNodes()) 
      newEl.appendChild(el.firstChild);
    el.parentNode.replaceChild(newEl, el);
  }
}
