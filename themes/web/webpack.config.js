"use strict";

var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

require("es6-promise").polyfill();

module.exports = {
  entry: [
    "babel-polyfill", "./src/main.js"
  ],

  output: {
    path: path.join(__dirname, "js"),
    filename: "scripts.js",
    publicPath: "/"
  },

  plugins: [
    new UglifyJsPlugin({
      test: /\.js($|\?)/i,
      exclude: /\/node_modules/,
      uglifyOptions: {
        ecma: 6,
        // compress: false,
        // mangle: false
      }
    }),
    // Specify the resulting CSS filename
    new ExtractTextPlugin("../css/style.css")
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["env"]
          }
        }
      }, {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                minimize: true
              }
            },
            "postcss-loader",
            "sass-loader"
          ]
        })
      }
    ]
  },

  stats: {
    // Colored output
    colors: true
  },

  // Create Sourcemaps for the bundle
  devtool: "source-map"
};
